// insert data
db.users.insertOne({
    "firstName": "Jane",
    "lastName": "Doe",
    "age": 21,
    "contact": {
        "phone": "123456789",
        "email": "janedoe@gmail.com",
    },
    "courses": ["CSS", "JavaScript", "Python"],
    "department": "HR"
});

db.users.insertMany([
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
            "phone": "123456",
            "email": "stephenhawking@gmail.com",
        },
        "courses": ["Python", "React", "PHP"],
        'department': "HR"
    },
    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
            "phone": "654321",
            "email": "neilarmstrong@gmail.com",
        },
        "courses": ["Python", "React", "PHP"],
        "department": "HR"
    }
]);

// Find users with the letter “s” in their first name or d in their last name.
db.users.find(
    {$or: [{"firstName": {$regex: "s", $options: "$i"}}, {"lastName": {$regex:"d", $options: "$i"}}]}, 
    {
        "firstName": 1,
        "lastName": 1,
        "_id": 0
    }
);

// Find users who are from the HR department and their age is greater than or equal to 70.
db.users.find({$and: [{"department": "HR"}, {"age": {$gte: 70}}]});

// Find users with the letter e in their first name and have an age of less than or equal to 30.
db.users.find({$and: [{"firstName": {$regex: "e"}}, {"age": {$lte: 30}}]});